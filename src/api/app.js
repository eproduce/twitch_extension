import qs from 'qs';
import request from '@/utils/request';

export function getStatusNowQuiz() {
  return request({
    url: '/getStatusNowQuiz',
    method: 'get',
  });
}

export function reloadAllForQuiz() {
  return request({
    url: '/reloadAllForQuiz',
    method: 'post',
  });
}

export function selectQuizForPlay(data) {
  const options = qs.stringify(data);

  return request({
    url: '/selectQuizForPlay',
    method: 'post',
    data: options,
  });
}

export function start(data) { // Получать уведомления по Email о следующей игре
  const options = qs.stringify(data);

  return request({
    url: '/start',
    method: 'post',
    data: options,
  });
}

export function getRaiting(params) {
  return request({
    url: '/getRaiting',
    method: 'get',
    params,
  });
}

export function createAcc() {
  return request({
    url: '/createAcc',
    method: 'post',
  });
}

export function deleteGuest(data) {
  const options = qs.stringify(data);

  return request({
    url: '/deleteGuest',
    method: 'post',
    data: options,
  });
}

export function registerGuest(data) {
  const options = qs.stringify(data);

  return request({
    url: '/registerGuest',
    method: 'post',
    data: options, // auth_key email
  });
}

export function signUp(data) {
  const options = qs.stringify(data);

  return request({
    // url: '/registerByEmail',
    url: '/registerGuest',
    method: 'post',
    data: options,
  });
}

export function registerCode(data) {
  const options = qs.stringify(data);

  return request({
    url: '/registerCode',
    method: 'post',
    data: options,
  });
}

export function registerCode2(data) {
  const options = qs.stringify(data);

  return request({
    url: '/registerCode2',
    method: 'post',
    data: options,
  });
}

export function signIn(data) {
  const options = qs.stringify(data);

  return request({
    url: '/authenticateByEmail',
    method: 'post',
    data: options,
  });
}
