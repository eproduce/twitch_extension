import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket';
import Vuelidate from 'vuelidate';
import vClickOutside from 'v-click-outside';
import svg4everybody from 'svg4everybody';
import { VLazyImagePlugin } from 'v-lazy-image';
import * as dayjs from 'dayjs';
import 'dayjs/locale/ru';
import App from './App.vue';
import router from './router';
import store from './store';

import 'normalize.css/normalize.css'; //  reset CSS
import '@/styles/layout.sass'; // global CSS

require('intersection-observer'); // for svg

Vue.config.productionTip = false;

Vue.use(Vuelidate);
Vue.use(vClickOutside);
svg4everybody();
Vue.use(VLazyImagePlugin);

Vue.prototype.$date = dayjs;
Vue.prototype.$date.locale('ru');

const ws = process.env.VUE_APP_NODE_ENV === 'production'
  ? process.env.VUE_APP_BASE_API_WS : process.env.VUE_APP_BASE_API_WS_DEV;

Vue.use(VueNativeSock, ws, {
  store,
  connectManually: true,
  reconnection: true,
  reconnectionDelay: 1000,
  format: 'json',
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
