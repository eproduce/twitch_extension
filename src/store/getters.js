const getters = {
  quizActive: (state) => state.app.quizActive,
  timeDelta: (state) => state.app.timeDelta,
  isPortrait: (state) => state.app.isPortrait,
  isMobile: (state) => state.app.isMobile,
  showMobileNav: (state) => state.app.showMobileNav,
  windowWidth: (state) => state.app.windowWidth,
  scrollY: (state) => state.app.scrollY,
  token: (state) => state.app.token,
  socket: (state) => state.app.socket,
  popup: (state) => state.app.popup,
  timeAnswer: (state) => state.app.timeAnswer,
  signUpEmail: (state) => state.app.signUpEmail,
  rating: (state) => state.app.rating,
  platform: (state) => state.app.platform,
  authTwitch: (state) => state.app.authTwitch,
  serverTime: (state) => state.app.serverTime,
};

export default getters;
