/* eslint-disable */
import Vue from 'vue';
import {
  registerCode2, signUp, signIn, registerGuest, deleteGuest, createAcc, start, getRaiting,
  reloadAllForQuiz, selectQuizForPlay, getStatusNowQuiz,
} from '@/api/app';
import { getToken, setToken, removeToken } from '@/utils/auth';
import router from '../../router';

const app = {
  state: {
    quizActive: null,
    timeDelta: null,
    authTwitch: null,
    platform: null,
    signUpEmail: null,
    token: getToken(),
    isPortrait: null,
    isMobile: null,
    showMobileNav: null,
    windowWidth: null,
    scrollY: null,
    popup: {
      opened: null,
      openedPopupContent: null,
      currentPopup: null,
      className: null,
      disabled: null,
      message: {
        head: null,
        body: null,
      },
      data: null,
    },
    socket: {
      serverTime: null,
      isConnected: false,
      reconnectError: false,
      timeQuiz: null,
      countOnline: 0,
      user: null,
      quizContent: null,
      answersCount: null,
      countCorrect: null,
      countIncorrect: null,
      canGame: null,
      raitingPlace: null,
      request: null,
      existRaiting: null,
    },
    timeAnswer: null,
    rating: {
      show: null,
      data: null,
    }
  },
  mutations: {
    UPDATE_TIME_DELTA: (state, time) => {
      state.timeDelta = time;
    },

    SET_AUTH_TWITCH: (state, authTwitch) => {
      state.authTwitch = authTwitch;
    },

    SET_PLATFORM: (state, platform) => {
      state.platform = platform;
    },

    SHOW_RATING: (state, rating) => {
      state.rating.show = rating;
    },

    UPDATE_RATING: (state, rating) => {
      state.rating.data = rating;
    },

    SET_SIGNUP_EMAIL: (state, email) => {
      state.signUpEmail = email;
    },

    SET_TOKEN: (state, token) => {
      state.token = token;

      setToken(token);
    },

    REMOVE_TOKEN: (state) => {
      state.token = null;

      removeToken();
    },

    UPDATE_TIME_ANSWER: (state, t) => {
      state.timeAnswer = t;
    },

    SHOW_POPUP_CONTENT: (state, open) => {
      state.popup.openedPopupContent = open;
    },

    SHOW_POPUP: (state, obj) => {
      state.popup.opened = obj.opened;
      state.popup.currentPopup = obj.currentPopup;
      state.popup.message = obj.message;
      state.popup.data = obj.data;
      state.popup.className = obj.className;
      state.popup.disabled = obj.disabled;
    },

    CHANGE_POPUP_CLASSNAME: (state, name) => {
      state.popup.className = name;
    },

    UPDATE_IS_MOBILE: (state, isMobile) => {
      state.isMobile = isMobile;
    },

    UPDATE_IS_PORTRAIT: (state, isPortrait) => {
      state.isPortrait = isPortrait;
    },

    UPDATE_WINDOW_WIDTH(state, width) {
      state.windowWidth = width
    },

    UPDATE_WINDOW_SCROLL_Y(state, val) {
      state.scrollY = val
    },

    SOCKET_ONOPEN(state, event) {
      if (window.location.hash !== '#/dashboard') {
        console.log('---', state.token);
        Vue.prototype.$socket.sendObj({
          type: 'register',
          auth_key: state.token,
        });
      }

      state.socket.isConnected = true;

      console.log('---', 'SOCKET_ONOPEN');
    },

    SOCKET_ONCLOSE (state, event)  {
      state.socket.isConnected = false;

      console.log('---', 'SOCKET_ONCLOSE');

      console.log('---', event);
    },

    SOCKET_ONERROR (state, event)  {
      state.socket.isConnected = false;

      console.log('---', 'SOCKET_ONERROR');

      console.log('---', event);
    },

    SOCKET_ONMESSAGE(state, message) {
      if (message.quizActive !== undefined) { // статус викторины
        state.quizActive = message.quizActive;
      }

      if (message.serverTime) { // вермя сервера для поправки
        state.socket.serverTime = message.serverTime;
      }

      if (message.noUser) { // не валидный токен
        state.token = null;

        removeToken();

        createAcc()
          .then((r) => {
            if (!r.error) {
              state.token = r.auth_key;
            }
          });
      }

      if (message.userProfile) { // профиль юзера
        state.socket.user = message.userProfile;
      }

      if (message.lifeCount) {
        if (state.socket.user) {
          state.socket.user.lifeCount = message.lifeCount;
        } else {
          state.socket.user = {
            lifeCount: message.lifeCount,
          }
        }
      }

      if (message.canGame === 1 || message.canGame === 0) { // запрет выбора ответа в викторине
        state.socket.canGame = message.canGame;
      }

      if (message.countOnline) { // всего в онлайне
        state.socket.countOnline = message.countOnline;
      }

      if (message.timeQuiz) { // время до старта викторины
        state.socket.timeQuiz = message.timeQuiz;
      }

      if (message.request === 'questInfo') { // старт викторины
        state.socket.request = message.request;

        state.socket.quizContent = message;

        state.socket.answersCount = null;

        state.socket.countCorrect = null;

        state.socket.countIncorrect = null;

        state.popup.opened = false;

        state.rating.show = false;

        if (window.location.hash !== '#/dashboard') {
          router.push('/quiz');
        }
      }

      if (message.request === 'questEnd') { // стоп викторины
        state.socket.request = message.request;

        state.socket.answersCount = message.answersCount;

        state.popup.opened = false;
      }

      if (message.request === 'showResultGood') { // ответ верный
        state.socket.request = message.request;

        state.socket.countCorrect = message.countCorrect;

        state.socket.countIncorrect = message.countIncorrect;

        if (window.location.hash !== '#/dashboard' && window.location.hash !== '#/wait') {
          router.push('/quiz-ok');
        }

        state.popup.opened = false;
      }

      if (message.request === 'showResultBad') { // ответ неверный но жизни еще есть
        state.socket.request = message.request;

        state.socket.countCorrect = message.countCorrect;

        state.socket.countIncorrect = message.countIncorrect;

        if (window.location.hash !== '#/dashboard' && window.location.hash !== '#/wait') {
          router.push('/quiz-error');
        }

        state.popup.opened = false;
      }

      if (message.request === 'showResultEnd') { // ответ неверный и жизни кончились
        state.socket.request = message.request;

        state.socket.countCorrect = message.countCorrect;

        state.socket.countIncorrect = message.countIncorrect;

        if (window.location.hash !== '#/dashboard' && window.location.hash !== '#/wait') {
          router.push('/quiz-error-not-live');
        }

        state.popup.opened = false;
      }

      if (message.request === 'showResultBad_noAnswer') { // афк, но жизни есть
        state.socket.countCorrect = message.countCorrect;

        state.socket.countIncorrect = message.countIncorrect;

        state.socket.request = message.request;

        if (window.location.hash !== '#/dashboard' && window.location.hash !== '#/wait') {
          router.push('/quiz-error');
        }

        state.popup.opened = false;
      }

      if (message.request === 'showResultEnd_noAnswer') { // афк, жизни кончились
        state.socket.countCorrect = message.countCorrect;

        state.socket.countIncorrect = message.countIncorrect;

        state.socket.request = message.request;

        if (window.location.hash !== '#/dashboard' && window.location.hash !== '#/wait') {
          router.push('/quiz-error-not-live');
        }

        state.popup.opened = false;
      }

      if (message.request === 'quizEnd') { // викторина завершена, вывод рейтинга
        state.socket.raitingPlace = message.raitingPlace;

        state.socket.request = message.request;

        state.socket.existRaiting = message.existRaiting;

        if (window.location.hash !== '#/dashboard') {
          router.push('/result');
        }
      }

      if (message.request === 'closeQuiz') { // закрыть викторину
        state.socket.request = message.request;

        if (window.location.hash !== '#/dashboard') {
          router.push('/');
        }
      }
    },

    SOCKET_RECONNECT(state, message) {
      state.socket.isConnected = false;

      console.log('---', 'SOCKET_RECONNECT');
    },

    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    },
  },
  actions: {
    GetStatusNowQuiz({ commit }) {
      return (async () => getStatusNowQuiz())();
    },

    SelectQuizForPlay({ commit }, data) {
      return (async () => selectQuizForPlay(data))();
    },

    ReloadAllForQuiz({ commit }) {
      return (async () => reloadAllForQuiz())();
    },

    GetRaiting({ commit }, data) {
      return (async () => getRaiting(data))();
    },

    Start({ commit }, data) {
      return (async () => start(data))();
    },

    CreateAccGuest({ commit }, data) {
      return (async () => createAcc(data))();
    },

    DeleteGuest({ commit }, data) {
      return (async () => deleteGuest(data))();
    },

    RegisterGuest({ commit }, data) {
      return (async () => registerGuest(data))();
    },

    SignUp({ commit }, data) {
      return (async () => signUp(data))();
    },

    RegisterCode({ commit }, data) {
      return (async () => registerCode2(data))();
    },

    SignIn({ commit }, data) {
      return (async () => signIn(data))();
    },
  },
};

export default app;
