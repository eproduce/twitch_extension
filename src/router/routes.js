const routes = [
  {
    path: '/',
    component: () => import('../layouts/Layout'),
    children: [
      { path: '', name: 'home', component: () => import('../views/Home') },
      { path: 'sign-up', name: 'sign-up', component: () => import('../views/SignUp') },
      { path: 'confirm', name: 'confirm', component: () => import('../views/Confirm') },
      { path: 'sign-in', name: 'sign-in', component: () => import('../views/SignIn') },
      { path: 'quiz', name: 'quiz', component: () => import('../views/Quiz') },
      { path: 'quiz-error', name: 'quiz-error', component: () => import('../views/QuizError') },
      {
        path: 'quiz-error-not-live',
        name: 'quiz-error-not-live',
        component: () => import('../views/QuizErrorNotLive'),
      },
      { path: 'quiz-ok', name: 'quiz-ok', component: () => import('../views/QuizOk') },
      { path: 'result', name: 'result', component: () => import('../views/Result') },
      { path: 'wait', name: 'wait', component: () => import('../views/Wait') },
      { path: 'dashboard', name: 'dashboard', component: () => import('../views/Dashboard') },
    ],
  },
  {
    path: '*',
    component: () => import('../layouts/Layout'),
    children: [
      { path: '', name: 'home', component: () => import('../views/Home') },
    ],
  },
];

export default routes;
